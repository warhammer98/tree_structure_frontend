import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'


let stompClient = null;
let handlers = [];

export function connect() {
    const socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/nodesActivity', function (message) {
            handlers.forEach( handler => {
                handler(JSON.parse(message.body));
            });
        });
    });
}

export function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

export function updateNode(node) {
    console.log("before send");
    console.log(node);
    stompClient.send("/app/updateNode", {}, JSON.stringify(node));
}
export function deleteNode(node) {
    console.log("before send");
    console.log(node);
    stompClient.send("/app/deleteNode", {}, JSON.stringify(node));
}


export function addHandler(handler) {
    handlers.push(handler);
}

/*
function cloneObject(object) {
    return JSON.parse(JSON.stringify(object));
}*/
