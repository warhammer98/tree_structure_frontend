import Vue from 'vue'

export function deleteNode(node, curParent) {
    if (curParent.children) {
        for (var i = 0; i < curParent.children.length; i++) {
            if (curParent.children[i].id == node.id) {
                curParent.children.splice(i, 1);
            }
            else {
                deleteNode(node, curParent.children[i])
            }
        }
    }
}
export function insertNode(node, curParent) {
    if (!curParent.children)
        Vue.set(curParent, 'children', []);
    let inserted = false;
    for (var i = 0; i < curParent.children.length; i++) {
        if (curParent.id == node.parentId) {
            if (curParent.children[i].id == node.id) {
                node.isOnFocus=false;
                Vue.set(curParent.children, i, node);
                inserted = true;
            }
        }
        else {
            insertNode(node, curParent.children[i]);
            if (curParent.children[i].id == node.id) {
                curParent.children.splice(i, 1);
            }
        }
    }
    if (!inserted) {
        if (curParent.id == node.parentId) {
            curParent.children.push(node);
        }
    }
}