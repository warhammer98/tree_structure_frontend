import Vue from 'vue'
import App from './App.vue'
import { connect } from "./util/ws";

connect();

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
